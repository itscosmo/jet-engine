import { registerPlugin } from './library';
import { render, getAttributes, appendOut } from './index';

function subroutine(chunk) {
  const attributes = getAttributes(chunk.attributes);
  const context = attributes.with ? attributes.with : attributes;
  appendOut(render(chunk.key, context));
}

registerPlugin('>', subroutine);
