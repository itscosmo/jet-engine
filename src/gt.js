// {@gt key=id value=selected_item} ... {:else} ... {/gt}
import { registerPlugin } from './library';
import { run, resolve } from './index';

function gt() {
  run(resolve('key') > resolve('value'), '{/gt}');
}

registerPlugin('@gt', gt);
