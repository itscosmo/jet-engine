const plugins = {};
const postProcessors = [];

export function registerPlugin(key, f) {
  plugins[key] = f;
}

export function getPlugin(key) {
  return plugins[key];
}

export function registerPostProcessor(f) {
  postProcessors.push(f);
}

export function postProcess() {
  postProcessors.forEach(f => f());
}
