// {@lte key=id value=selected_item} ... {:else} ... {/lte}
import { registerPlugin } from './library';
import { run, resolve } from './index';

function lte() {
  run(resolve('key') <= resolve('value'), '{/lte}');
}

registerPlugin('@lte', lte);
