// {@eq key=id value=selected_item} ... {:else} ... {/eq}
import { registerPlugin } from './library';
import { run, resolve } from './index';

function eq() {
  run(resolve('key') === resolve('value'), '{/eq}');
}

registerPlugin('@eq', eq);
