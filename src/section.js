import { registerPlugin } from './library';
import {
  pushStack,
  popStack,
  get,
  runUntil,
  skipUntil,
  getPtr,
  setPtr,
  pushSpecial,
  setSpecial,
  popSpecial,
  getAttributes,
} from './index';

function section(chunk) {
  const id = chunk.key;
  const attributes = getAttributes(chunk.attributes);
  let value = get(id);
  const ptr = getPtr();
  const endTag = `{/${id}}`;
  const elseTag = '{:else}';
  const truth = !(value === '' || value === false || value === null || value === undefined) &&
    !(Array.isArray(value) && value.length === 0);

  if (truth) {
    if (!Array.isArray(value)) {
      value = [value];
    }
    pushSpecial({ $len: value.length });
    pushStack(attributes);
    for (let i = 0, ln = value.length; i < ln; i++) {
      setSpecial('$idx', i);
      setSpecial('$idx1', i + 1);
      pushStack(value[i]);
      const until = runUntil([endTag, elseTag]);
      if (until === elseTag) {
        skipUntil(endTag);
      }
      popStack();
      if (i < value.length - 1) {
        setPtr(ptr);
      }
    }
    popStack();
    popSpecial();
  } else {
    const until = skipUntil([endTag, elseTag]);
    if (until === elseTag) {
      runUntil(endTag);
    }
  }
}

registerPlugin('#', section);
