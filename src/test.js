import { registerPlugin } from './library';
import { run, get } from './index';

function test(chunk) {
  const id = chunk.key;
  const value = get(id);
  const truth = !(value === '' || value === false || value === null || value === undefined) &&
    !(Array.isArray(value) && value.length === 0);

  run(truth, `{/${id}}`);
}

registerPlugin('?', test);
