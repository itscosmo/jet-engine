import { registerPlugin } from './library';
import { appendOut } from './index';

function special(chunk) {
  const id = chunk.key;
  switch (id) {
    case 'n':
      appendOut('\n');
      break;
    case 'r':
      appendOut('\r');
      break;
    case 's':
      appendOut(' ');
      break;
    case 'lb':
      appendOut('{');
      break;
    case 'rb':
      appendOut('}');
      break;
    case 'lt':
      appendOut('<');
      break;
    case 'gt':
      appendOut('>');
      break;
    case 'a':
      appendOut("'");
      break;
    case 'q':
      appendOut('"');
      break;
    default:
      break;
  }
}

registerPlugin('~', special);
