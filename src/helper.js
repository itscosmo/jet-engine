import { getPlugin, registerPlugin } from './library';
import { popStack, pushStack } from './index';

function helper(chunk) {
  const keys = Object.keys(chunk.attributes);

  if (keys.length) {
    pushStack(chunk.attributes);
  }
  try {
    getPlugin(chunk.id)(chunk.attributes);
  } catch (err) {
    throw new Error(`Unable to execute helper ${chunk.id}`);
  } finally {
    if (keys.length) {
      popStack();
    }
  }
}

registerPlugin('@', helper);
