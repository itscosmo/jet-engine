// {@gte key=id value=selected_item} ... {:else} ... {/gte}
import { registerPlugin } from './library';
import { run, resolve } from './index';

function gte() {
  run(resolve('key') >= resolve('value'), '{/gte}');
}

registerPlugin('@gte', gte);
