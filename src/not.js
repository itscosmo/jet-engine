import { registerPlugin } from './library';
import { get, run } from './index';

function not(chunk) {
  const id = chunk.key;
  const value = get(id);
  run(!value, `{/${id}}`);
}

registerPlugin('!', not);
