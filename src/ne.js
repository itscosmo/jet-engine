// {@ne key=id value=selected_item} ... {:else} ... {/ne}
import { registerPlugin } from './library';
import { run, resolve } from './index';

function ne() {
  run(resolve('key') !== resolve('value'), '{/ne}');
}

registerPlugin('@ne', ne);
