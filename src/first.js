// {@first} ... {:else} ... {/first}
import { registerPlugin } from './library';
import { run, get } from './index';

function first() {
  run(get('$idx') === 0, '{/first}');
}

registerPlugin('@first', first);
