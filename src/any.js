import { registerPlugin } from './library';
import { run, getSpecial } from './index';

function any() {
  run(getSpecial('$select') === true, '{/any}');
}

registerPlugin('@any', any);
