// TODO test {.[0]}, {[0]}
// TODO math formatting, e.g. 6, 6.0, 6.00
import { getPlugin, postProcess } from './library';
import './plugins';

const ELSE_TAG = '{:else}';

const stack = [];
const registry = {};
const outputStack = [];
const templateStack = [];
const ptrStack = [];
const specialStack = [];
let template;

export function appendOut(s) {
  outputStack[outputStack.length - 1] += s;
}

export function pushOutput(s) {
  outputStack.push(s);
}

export function popOutput() {
  return outputStack.pop();
}

export function pushSpecial(o) {
  specialStack.push(o);
}

export function setSpecial(key, value) {
  specialStack[specialStack.length - 1][key] = value;
}

export function getSpecial(key) {
  return specialStack.length ? specialStack[specialStack.length - 1][key] : '';
}

export function popSpecial() {
  return specialStack.pop();
}

export function pushTemplate(templ) {
  templateStack.push(templ);
  ptrStack.push(0);
  template = templ;
}

export function popTemplate() {
  ptrStack.pop();
  const lastTemplate = templateStack.pop();
  if (templateStack.length > 0) {
    template = templateStack[templateStack.length - 1];
  }
  return lastTemplate;
}

export function getTemplate() {
  // return templateStack[templateStack.length - 1];
  return template;
}

export function incPtr() {
  const ptr = ptrStack[ptrStack.length - 1];
  ptrStack[ptrStack.length - 1] = ptr + 1;
  return ptr;
}

export function getPtr() {
  return ptrStack[ptrStack.length - 1];
}

export function setPtr(ptr) {
  ptrStack[ptrStack.length - 1] = ptr;
}

export function pushStack(context) {
  stack.push(context);
}

export function popStack() {
  return stack.pop();
}

// export function dumpStack() {
//   console.log('========== STACK',);
//   stack.forEach(value => {
//     console.log(value);
//     console.log('==========',);
//   });
// }
//
export function getAttributes(attr) {
  const attributes = {};
  const keys = Object.keys(attr);
  const len = keys.length;
  for (let i = 0; i < len; i++) {
    const key = keys[i];
    attributes[key] = get(attr[key]);
  }
  return attributes;
}

export function parseDirective(chunkValue) {
  let value = chunkValue;
  if (value.slice(-1) === '/') {
    value = value.slice(0, -1).trim();
  }
  const parts = [];
  const attributes = {};
  let isSpace = false;
  let quote = null;
  let part = '';
  for (let i = 0; i < value.length; i++) {
    const c = value.charAt(i);
    if (c === '"' || c === "'") {
      if (c === quote) {
        quote = null;
      } else if (!quote) {
        quote = c;
      }
    }

    if (c === ' ' && !quote) {
      isSpace = true;
    } else {
      if (isSpace) {
        parts.push(part);
        part = '';
      }
      isSpace = false;
    }

    if (!isSpace) {
      part += c;
    }
  }
  parts.push(part);
  for (let i = 1; i < parts.length; i++) {
    part = parts[i];
    const idx = part.indexOf('=');
    attributes[part.slice(0, idx)] = part.slice(idx + 1);
  }
  return {
    id: parts[0],
    attributes,
  };
}

function closeQuote(id, quote, endQuote) {
  let level = 0;
  for (let i = 0; i < id.length; i++) {
    const c = id.charAt(i);
    switch (c) {
      case quote:
        level++;
        break;
      case endQuote:
        level--;
        if (level === 0) {
          return i;
        }
        break;
      default:
        break;
    }
  }
  return -1;
}

function getValue(id, o) {
  if (o === undefined) {
    return o;
  }

  const c = id.charAt(0);
  switch (c) {
    case '':
      return o;
    case "'":
    case '"': {
      const endQuote = id.indexOf(c, 1);
      if (endQuote === -1) {
        throw new Error(`Mismatched quote (${c})`);
      }
      return getValue(id.slice(endQuote + 1), id.slice(1, endQuote));
    }
    case '.':
      return getValue(id.slice(1), o);
    case '[': {
      const iClose = closeQuote(id, '[', ']');
      if (iClose === -1) {
        throw new Error('Mismatched brackets');
      }
      return getValue(id.slice(iClose + 1), o[get(id.slice(1, iClose))]);
    }
    default: {
      const iDot = id.indexOf('.');
      const iOpen = id.indexOf('[');
      if (iDot === -1 && iOpen === -1) {
        return o[id];
      }
      if (iDot >= 0 && (iOpen === -1 || iOpen > iDot)) {
        return getValue(id.slice(iDot), o[id.slice(0, iDot)]);
      }
      return getValue(id.slice(iOpen), o[id.slice(0, iOpen)]);
    }
  }
}

export function resolve(key) {
  return get(get(key));
}

export function get(id) {
  if (!Number.isNaN(parseFloat(id))) {
    return parseFloat(id);
  }

  if (id === '.') {
    return stack[stack.length - 1];
  }

  if (id.charAt(0) === '$') {
    return getSpecial(id);
  }

  for (let i = stack.length - 1; i >= 0; i--) {
    const o = stack[i];
    const value = getValue(id, o);
    if (value !== undefined) {
      return value;
    }
    if (id[0] === '.') {
      return '';
    }
  }
  return '';
}

function nextChunk() {
  const ptr = incPtr();
  const chunk = getTemplate()[ptr];
  return chunk;
}

function handleValue(chunk) {
  appendOut(get(chunk.value.slice(1, -1)));
}

function handleDirective(chunk) {
  const key = chunk.id ? chunk.id[0] : '';
  if (getPlugin(key)) {
    getPlugin(key)(chunk);
  } else {
    handleValue(chunk);
  }
}

function handleLiteral(chunk) {
  appendOut(chunk.value);
}

function handleChunk() {
  const chunk = nextChunk();
  if (chunk.value.charAt(0) === '{') {
    handleDirective(chunk);
  } else {
    handleLiteral(chunk);
  }
  return chunk;
}

export function processUntil(directives, process) {
  const list = Array.isArray(directives) ? directives : [directives];
  let result = { returnVal: [] };

  do {
    result = process(result); // { chunk, returnVal }
  } while (list.indexOf(result.chunk.value) === -1 && result.chunk.value !== '');

  if (list.indexOf(result.chunk.value) === -1) {
    throw new Error(`Mismatched tag: ${list[0]}`);
  }

  return result.returnVal;
}

export function readUntil(directives) {
  return processUntil(directives, (result) => {
    result.chunk = nextChunk();
    result.returnVal.push(result.chunk);
    return result;
  });
}

export function skipUntil(directives) {
  return processUntil(directives, (result) => {
    result.chunk = nextChunk();
    result.returnVal = result.chunk.value;
    return result;
  });
}

export function runUntil(directives) {
  return processUntil(directives, (result) => {
    result.chunk = handleChunk();
    result.returnVal = result.chunk.value;
    return result;
  });
}

export function run(condition, end) {
  if (condition) {
    if (runUntil([end, ELSE_TAG]) === ELSE_TAG) {
      skipUntil(end);
    }
  } else if (skipUntil([end, ELSE_TAG]) === ELSE_TAG) {
    runUntil(end);
  }
}

export function compress(s) {
  const isText = s.slice(0, 7) === '{%TEXT}';
  const isHTML = s.slice(0, 7) === '{%HTML}';
  if (!isHTML && s.charAt(0) !== '<') {
    return (isText ? s.slice(7) : s).replace(/{\*.*?\*}/g, '');
  }

  return (isHTML ? s.slice(7) : s)
    .replace(/\n\s*/g, ' ')
    .replace(/\s*</g, '<')
    .replace(/>\s*/g, '>')
    .replace(/{\*.*?\*}/g, '')
    .trim();
}

export function compile(t) {
  // if its an Array, assume it's already compiled;
  if (Array.isArray(t)) {
    return t;
  }
  let templ = compress(t);

  function getFragment() {
    const idx = templ.indexOf('{');
    let value;

    if (idx === 0) {
      const idx2 = templ.indexOf('}');
      value = templ.slice(0, idx2 + 1);
      templ = templ.slice(idx2 + 1);
    } else if (idx === -1) {
      value = templ;
      templ = '';
    } else {
      value = templ.slice(0, idx);
      templ = templ.slice(idx);
    }

    return value;
  }

  const result = [];
  let fragment;
  let dir;
  let chunk;

  do {
    fragment = getFragment();
    if (fragment.charAt(0) === '{' && getPlugin(fragment.charAt(1))) {
      dir = parseDirective(fragment.slice(1, -1));
      chunk = {
        value: fragment,
        id: dir.id,
        key: dir.id.slice(1),
        attributes: dir.attributes,
      };
    } else {
      chunk = { value: fragment };
    }
    result.push(chunk);
  } while (fragment !== '');

  return result;
}

export function isSelfClosing(chunk) {
  return chunk.value.slice(-2) === '/}';
}

export function endTag(id) {
  return `{/${id}}`;
}

export function render(t, data) {
  const templ = registry[t] ? registry[t] : compile(t);
  pushOutput('');
  pushTemplate(templ);
  pushStack(data);
  try {
    runUntil('');
  } catch (err) {
    cleanup();
    throw err;
  }
  popStack();
  popTemplate();
  if (outputStack.length === 1) {
    postProcess();
  }
  return (popOutput());
}

export function register(name, t) {
  registry[name] = compile(t);
}

export function isRegistered(name) {
  return !!registry[name];
}

export function cleanup() {
  stack.length = 0;
  outputStack.length = 0;
  templateStack.length = 0;
  ptrStack.length = 0;
  specialStack.length = 0;
}
