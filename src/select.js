import { registerPlugin } from './library';
import { run, pushOutput, pushSpecial, popSpecial, getPtr, setPtr, popOutput } from './index';

function select() {
  const ptr = getPtr();
  pushOutput('');
  run(true, '{/select}');
  pushSpecial({ $select: popOutput().length > 0 });
  setPtr(ptr);
  run(true, '{/select}');
  popSpecial();
}

registerPlugin('@select', select);
