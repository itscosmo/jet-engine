import { compress } from './index';

module.exports = function (content) {
  return `module.exports = ${JSON.stringify(compress(content))};`;
};

