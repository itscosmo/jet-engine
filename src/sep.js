// {@sep} ... {:else} ... {/sep}
import { registerPlugin } from './library';
import { run, get } from './index';

function sep() {
  run(get('$idx1') !== get('$len'), '{/sep}');
}

registerPlugin('@sep', sep);
