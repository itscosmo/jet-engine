import { registerPlugin } from './library';
import { render, register, readUntil, getAttributes, getSpecial } from './index';
import { setRef, getRef } from './ref';

function define(chunk) {
  const id = chunk.key;
  const attributes = getAttributes(chunk.attributes);
  const result = readUntil(`{/${id}}`);
  result.pop();
  result.push({ value: '' });
  register(id, result);
  const index = getSpecial('$idx');
  if (index !== '') {
    if (index === 0) {
      setRef(id, '');
    }
    setRef(id, `${getRef(id)}${render(id, attributes)}`);
  } else {
    setRef(id, render(id, attributes));
  }
}

registerPlugin('<', define);
