import { registerPlugin } from './library';
import { appendOut, resolve } from './index';

function math() {
  const rawKey = resolve('key');
  const key = parseFloat(rawKey);
  const method = resolve('method');
  const operand = parseFloat(resolve('operand'));

  switch (method) {
    case '+':
    case 'add':
      appendOut(`${key + operand}`);
      break;
    case '-':
    case 'subtract':
      appendOut(`${key - operand}`);
      break;
    case '*':
    case 'multiply':
      appendOut(`${key * operand}`);
      break;
    case '/':
    case 'divide':
      appendOut(`${key / operand}`);
      break;
    case '%':
    case 'mod':
      appendOut(`${key % operand}`);
      break;
    case '**':
    case '^':
    case 'pow':
      appendOut(`${key ** operand}`);
      break;
    case 'ceil':
      appendOut(`${Math.ceil(key)}`);
      break;
    case 'floor':
      appendOut(`${Math.floor(key)}`);
      break;
    case 'round':
      appendOut(`${Math.round(key)}`);
      break;
    case 'toint':
      appendOut(`${parseInt(rawKey, 10)}`);
      break;
    case 'abs':
      appendOut(`${Math.abs(key)}`);
      break;
    default:
      break;
  }
}

registerPlugin('@math', math);
