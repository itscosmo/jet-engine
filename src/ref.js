import { registerPlugin, registerPostProcessor } from './library';
import {
  isRegistered,
  render,
  isSelfClosing,
  pushOutput,
  runUntil,
  popOutput,
  appendOut,
  endTag,
} from './index';

const refs = {};

export function setRef(id, value) {
  refs[id] = value;
}

export function getRef(id) {
  return refs[id];
}

function ref(chunk) {
  const id = chunk.key;
  pushOutput('');
  if (!isSelfClosing(chunk)) {
    runUntil(endTag(id));
  }
  setRef(id, popOutput());

  if (isRegistered(id)) {
    setRef(id, render(id, {}));
  }
  appendOut(`{+${id}}`);
}

function post() {
  let out = popOutput();
  Object.keys(refs).forEach((key) => {
    const tag = `{\\+${key}}`;
    out = out.replace(new RegExp(tag, 'g'), refs[key]);
  });
  pushOutput(out);
}

registerPlugin('+', ref);
registerPostProcessor(post);
