import { registerPlugin } from './library';
import { run, getSpecial } from './index';

function none() {
  run(getSpecial('$select') === false, '{/none}');
}

registerPlugin('@none', none);
