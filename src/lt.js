// {@lt key=id value=selected_item} ... {:else} ... {/lt}
import { registerPlugin } from './library';
import { run, resolve } from './index';

function lt() {
  run(resolve('key') < resolve('value'), '{/lt}');
}

registerPlugin('@lt', lt);
