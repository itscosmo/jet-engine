// {@last} ... {:else} ... {/last}
import { registerPlugin } from './library';
import { run, get } from './index';

function last() {
  run(get('$idx1') === get('$len'), '{/last}');
}

registerPlugin('@last', last);
