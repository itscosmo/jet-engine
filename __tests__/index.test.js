import { compress, render, register } from '../src';
import loader from '../src/loader';
import { registerPlugin } from '../src/library';

describe('Jet Engine', () => {
  describe('compress', () => {
    // it('should exampe1', () => {
    //   const data = {
    //     last: 'Smith',
    //     names: [
    //       {
    //         first: 'Robert',
    //         age: 32,
    //
    //       },
    //       {
    //         first: 'William',
    //       },
    //       {
    //         first: 'Marcus',
    //         last: 'Smythe',
    //         age: 72,
    //       },
    //     ],
    //   };
    //
    //   const out = render(
    //     'The {last} Family:{~n}' +
    //     '{#names}' +
    //     '  {$idx1}: {first} {last}{?age}, age {age}{/age}{~n}' +
    //     '{/names}',
    //     data,
    //   );
    //
    //   console.log('**** OUT=\n', out);
    // });
    //
    it('should remove comments', () => {
      expect(compress('{*a short comment*}Hello')).toEqual('Hello');
    });

    it('should load a template', () => {
      expect(loader('This is a {* short *}test')).toEqual('module.exports = "This is a test";');
    });
  });

  it('should handle {%TEXT} directive', () => {
    const out = render(
      '{%TEXT}<   Wow   >    <   Wow   >!',
      {},
    );
    expect(out).toEqual('<   Wow   >    <   Wow   >!');
  });

  it('should handle {%HTML} directive', () => {
    const out = render(
      '{%HTML}{?test}<input type="password">     <p>Hello</p>{/test}',
      { test: true },
    );
    expect(out).toEqual('<input type="password"><p>Hello</p>');
  });

  it('should parse complex object', () => {
    const out = render(
      'Hello, {a.b[c.d][e].f}!',
      { a: { b: [[2, 3, 4, 5], [{ g: 'Bad' }, 7, { f: 'Surprise' }, { f: 'Bad' }]] }, c: { d: 1 }, e: 2 },
    );
    expect(out).toEqual('Hello, Surprise!');
  });

  it('should parse literal string', () => {
    const out = render(
      '[{"literal"[5]}]',
      {},
    );
    expect(out).toEqual('[a]');
  });

  it('should access object with array notation', () => {
    expect(render(
      "Hello, {name['janitor']}!",
      { name: { manager: 'Jim', janitor: 'Joy' } },
    )).toEqual('Hello, Joy!');
  });

  it('should perform a positive test', () => {
    const out = render(
      '{?show}Hello, {/show}World!',
      { show: true },
    );
    expect(out).toEqual('Hello, World!');
  });

  it('should perform a positive else test', () => {
    const out = render(
      '{?show}Hello, {:else}Go away, {/show}World!',
      { show: true },
    );
    expect(out).toEqual('Hello, World!');
  });

  it('should perform a negative test', () => {
    const out = render(
      '{?show}Hello, {/show}World!',
      { show: false },
    );
    expect(out).toEqual('World!');
  });

  it('should perform a negative else test', () => {
    const out = render(
      '{?show}Hello, {:else}Go away, {/show}World!',
      { show: false },
    );
    expect(out).toEqual('Go away, World!');
  });

  it('should perform "not" comparison', () => {
    const out = render(
      'Now {!show}not shown{/show}.',
      { show: false },
    );
    expect(out).toEqual('Now not shown.');
  });

  it('should perform "not" comparison (dustjs)', () => {
    const out = render(
      'Now {^show}not shown{/show}.',
      { show: false },
    );
    expect(out).toEqual('Now not shown.');
  });

  it('should perform "not" comparison (negative)', () => {
    const out = render(
      'Now {!show}Not shown{/show}.',
      { show: true },
    );
    expect(out).toEqual('Now .');
  });

  it('should perform "not" comparison else', () => {
    const out = render(
      '{!show}Not shown{:else}Shown{/show}.',
      { show: false },
    );
    expect(out).toEqual('Not shown.');
  });

  it('should perform "not" comparison (negative) else', () => {
    const out = render(
      '{!show}Not shown{:else}Shown{/show}.',
      { show: true },
    );
    expect(out).toEqual('Shown.');
  });

  it('should loop through a section', () => {
    const out = render(
      'Hello, {#names}{.}({key}-{$idx}/{$len}) {:else}will not be shown{/names}!',
      { key: 'Secret', names: ['Jim', 'Bob', 'Joe'] },
    );
    expect(out).toEqual('Hello, Jim(Secret-0/3) Bob(Secret-1/3) Joe(Secret-2/3) !');
  });

  it('should set context for a non-Array section', () => {
    const out = render(
      'Hello, {#names}{.}({key}-{$idx}/{$len}) {/names}!',
      { key: 'Secret', names: 'Burt' },
    );
    expect(out).toEqual('Hello, Burt(Secret-0/1) !');
  });

  it('should process else part of section', () => {
    const out = render(
      'Hello, {#planets}{.}({key}-{$idx}/{$len}) {:else}Empty Space{/planets}!',
      { key: 'Secret', names: 'Burt' },
    );
    expect(out).toEqual('Hello, Empty Space!');
  });

  it('should loop through a section (deep)', () => {
    const out = render(
      'Hello, {#values.names}{.}({key}) {/values.names}!',
      { key: 'Secret', values: { names: ['Jim', 'Bob', 'Joe'] } },
    );
    expect(out).toEqual('Hello, Jim(Secret) Bob(Secret) Joe(Secret) !');
  });

  it('should do nothing for empty section', () => {
    const out = render(
      'Hello, {#names}{.}({key}-{$idx}/{$len}) {/names}!',
      { key: 'Secret' },
    );
    expect(out).toEqual('Hello, !');
  });

  it('should accept attributes for loop', () => {
    const out = render(
      'Hello, {#names last="Smith"}{first} {last}{@sep}, {/sep}{/names}!',
      { names: [{ first: 'Jim' }, { first: 'Jane' }] },
    );
    expect(out).toEqual('Hello, Jim Smith, Jane Smith!');
  });

  it('should have else clause for loop', () => {
    const out = render(
      'Hello, {#names}{.}({key}-{$idx}/{$len}) {:else}No Name{/names}!',
      { key: 'Secret' },
    );
    expect(out).toEqual('Hello, No Name!');
  });

  it('should support @eq (positive test)', () => {
    const out = render(
      'Hello, {@eq key=title value="Mr."}Sir{:else}Ma&apos;am{/eq}!',
      { title: 'Mr.' },
    );
    expect(out).toEqual('Hello, Sir!');
  });

  it('should support @eq (negative test)', () => {
    const out = render(
      'Hello, {@eq key=title value="Mr."}Sir{:else}Ma&apos;am{/eq}!',
      { title: 'Mrs.' },
    );
    expect(out).toEqual('Hello, Ma&apos;am!');
  });

  it('should support @eq no else (positive)', () => {
    const out = render(
      'Hello{@eq key=title value="Mr."}, Sir{/eq}!',
      { title: 'Mr.' },
    );
    expect(out).toEqual('Hello, Sir!');
  });

  it('should support @eq no else (negative)', () => {
    const out = render(
      'Hello{@eq key=title value="Mr."}, Sir{/eq}!',
      { title: 'Mrs.' },
    );
    expect(out).toEqual('Hello!');
  });

  it('should support @ne (positive test)', () => {
    const out = render(
      'Hello, {@ne key=title value="Mr."}Miss{/ne}!',
      { title: 'Mr.' },
    );
    expect(out).toEqual('Hello, !');
  });

  it('should support @ne (negative test)', () => {
    const out = render(
      'Hello, {@ne key=title value="Mr."}Miss{/ne}!',
      { title: 'Mrs.' },
    );
    expect(out).toEqual('Hello, Miss!');
  });

  it('should support @ne (positive test) else', () => {
    const out = render(
      'Hello, {@ne key=title value="Mr."}Miss{:else}Sir{/ne}!',
      { title: 'Mr.' },
    );
    expect(out).toEqual('Hello, Sir!');
  });

  it('should support @ne (negative test) else', () => {
    const out = render(
      'Hello, {@ne key=title value="Mr."}Miss{:else}Sir{/ne}!',
      { title: 'Mrs.' },
    );
    expect(out).toEqual('Hello, Miss!');
  });

  it('should support subroutines', () => {
    register('name', 'Hello, {name}!');
    const out = render(
      'list: {>name name="Jim"} {>name name="Bob"}...',
      { title: 'Mrs.' },
    );
    expect(out).toEqual('list: Hello, Jim! Hello, Bob!...');
  });

  it('should support subroutines using with keyword', () => {
    register('name', '{last}, {first}');
    const out = render(
      'You are {>name with=person}.',
      { person: { first: 'Bill', last: 'Jones' } },
    );
    expect(out).toEqual('You are Jones, Bill.');
  });

  it('should support @first keyword', () => {
    register('name', '{last}, {first}');
    const out = render(
      '{#names}{@first}Hey, {/first}{.} {/names}!',
      { names: ['Jimmie', 'Stevie'] },
    );
    expect(out).toEqual('Hey, Jimmie Stevie !');
  });

  it('should support @first keyword (else)', () => {
    register('name', '{last}, {first}');
    const out = render(
      'Hey {#names}{.}{@first} and {:else} Smith {/first}{/names}!',
      { names: ['Jimmie', 'Stevie', 'Marylou', 'Jenny'] },
    );
    expect(out).toEqual('Hey Jimmie and Stevie Smith Marylou Smith Jenny Smith !');
  });

  it('should support @last keyword', () => {
    register('name', '{last}, {first}');
    const out = render(
      'Hey {#names}{.} {@last}Smith{/last}{/names}!',
      { names: ['Jimmie', 'Stevie', 'Marylou', 'Jenny'] },
    );
    expect(out).toEqual('Hey Jimmie Stevie Marylou Jenny Smith!');
  });

  it('should support @last keyword else', () => {
    register('name', '{last}, {first}');
    const out = render(
      'Hey {#names}{.}{@last} Smith {:else} and {/last}{/names}!',
      { names: ['Jimmie', 'Stevie', 'Marylou', 'Jenny'] },
    );
    expect(out).toEqual('Hey Jimmie and Stevie and Marylou and Jenny Smith !');
  });

  it('should support @sep keyword', () => {
    register('name', '{last}, {first}');
    const out = render(
      'Hey {#names}{.}{@sep}-{/sep}{/names}!',
      { names: ['Jimmie', 'Stevie', 'Marylou', 'Jenny'] },
    );
    expect(out).toEqual('Hey Jimmie-Stevie-Marylou-Jenny!');
  });

  it('should support @sep keyword else', () => {
    register('name', '{last}, {first}');
    const out = render(
      'Hey {#names}{.}{@sep}-{:else}-last{/sep}{/names}!',
      { names: ['Jimmie', 'Stevie', 'Marylou', 'Jenny'] },
    );
    expect(out).toEqual('Hey Jimmie-Stevie-Marylou-Jenny-last!');
  });

  it('should support @gt (positive test)', () => {
    const out = render(
      'It\'s {@gt key=size value=4}Large{/gt}',
      { size: 6 },
    );
    expect(out).toEqual('It\'s Large');
  });

  it('should support @gt (negative test)', () => {
    const out = render(
      'It\'s {@gt key=size value=4}Large{/gt}',
      { size: 4 },
    );
    expect(out).toEqual('It\'s ');
  });

  it('should support @gt (positive test) else', () => {
    const out = render(
      'It\'s {@gt key=size value=4}Large{:else}Small{/gt}',
      { size: 6 },
    );
    expect(out).toEqual('It\'s Large');
  });

  it('should support @gt (negative test) else', () => {
    const out = render(
      'It\'s {@gt key=size value=4}Large{:else}Small{/gt}',
      { size: 4 },
    );
    expect(out).toEqual('It\'s Small');
  });

  it('should support @gte (positive test)', () => {
    const out = render(
      'It\'s {@gte key=size value=6}Large{/gte}',
      { size: 6 },
    );
    expect(out).toEqual('It\'s Large');
  });

  it('should support @gte (negative test)', () => {
    const out = render(
      'It\'s {@gte key=size value=6}Large{/gte}',
      { size: 5 },
    );
    expect(out).toEqual('It\'s ');
  });

  it('should support @gte (positive test) else', () => {
    const out = render(
      'It\'s {@gte key=size value=6}Large{:else}Small{/gte}',
      { size: 6 },
    );
    expect(out).toEqual('It\'s Large');
  });

  it('should support @gte (negative test) else', () => {
    const out = render(
      'It\'s {@gte key=size value=6}Large{:else}Small{/gte}',
      { size: 5 },
    );
    expect(out).toEqual('It\'s Small');
  });

  it('should support @lt (positive test)', () => {
    const out = render(
      'It\'s {@lt key=size value=6}Small{/lt}',
      { size: 5 },
    );
    expect(out).toEqual('It\'s Small');
  });

  it('should support @lt (negative test)', () => {
    const out = render(
      'It\'s {@lt key=size value=6}Small{/lt}',
      { size: 6 },
    );
    expect(out).toEqual('It\'s ');
  });

  it('should support @lt (positive test) else', () => {
    const out = render(
      'It\'s {@lt key=size value=6}Small{:else}Large{/lt}',
      { size: 5 },
    );
    expect(out).toEqual('It\'s Small');
  });

  it('should support @lt (negative test) else', () => {
    const out = render(
      'It\'s {@lt key=size value=6}Small{:else}Large{/lt}',
      { size: 6 },
    );
    expect(out).toEqual('It\'s Large');
  });

  it('should support @lte (positive test)', () => {
    const out = render(
      'It\'s {@lte key=size value=5}Small{/lte}',
      { size: 5 },
    );
    expect(out).toEqual('It\'s Small');
  });

  it('should support @lte (negative test)', () => {
    const out = render(
      'It\'s {@lte key=size value=5}Small{/lte}',
      { size: 6 },
    );
    expect(out).toEqual('It\'s ');
  });

  it('should support @lte (positive test) else', () => {
    const out = render(
      'It\'s {@lte key=size value=5}Small{:else}Large{/lte}',
      { size: 5 },
    );
    expect(out).toEqual('It\'s Small');
  });

  it('should support @lte (negative test) else', () => {
    const out = render(
      'It\'s {@lte key=size value=5}Small{:else}Large{/lte}',
      { size: 6 },
    );
    expect(out).toEqual('It\'s Large');
  });

  it('should support special characters', () => {
    const out = render(
      '{~lb}{~q}{~ho}A{~yo}{~q}{~rb}{~s}{~lt}{~a}Single{~a}{~gt}{~r}{~n}',
      {},
    );
    expect(out).toEqual("{\"A\"} <'Single'>\r\n");
  });

  it('should support addition', () => {
    const out = render(
      'You are {@math key=age method="+" operand=5.5}',
      { age: 20 },
    );
    expect(out).toEqual('You are 25.5');
  });

  it('should support subtraction', () => {
    const out = render(
      'You are {@math key=age method="-" operand=5.5}',
      { age: 20 },
    );
    expect(out).toEqual('You are 14.5');
  });

  it('should support multiplication', () => {
    const out = render(
      'You are {@math key=age method="*" operand=2.5}',
      { age: 20 },
    );
    expect(out).toEqual('You are 50');
  });

  it('should support division', () => {
    const out = render(
      'You are {@math key=age method="/" operand=4}',
      { age: 20 },
    );
    expect(out).toEqual('You are 5');
  });

  it('should support exponentiation', () => {
    const out = render(
      'Volume of cube is {@math key=size method="**" operand=3}',
      { size: 2 },
    );
    expect(out).toEqual('Volume of cube is 8');
  });

  it('should support modulus', () => {
    const out = render(
      'You are {@math key=age method="%" operand=3}',
      { age: 20 },
    );
    expect(out).toEqual('You are 2');
  });

  it('should support ceiling', () => {
    const out = render(
      'You are {@math key=age method="ceil"}',
      { age: 20.1 },
    );
    expect(out).toEqual('You are 21');
  });

  it('should support floor', () => {
    const out = render(
      'You are {@math key=age method="floor"}',
      { age: 20.6 },
    );
    expect(out).toEqual('You are 20');
  });

  it('should support rounding', () => {
    const out = render(
      'You are {@math key=age method="round"}',
      { age: 20.4 },
    );
    expect(out).toEqual('You are 20');
  });

  it('should support toint', () => {
    const out = render(
      'You are {@math key=age method="toint"}',
      { age: '23' },
    );
    expect(out).toEqual('You are 23');
  });

  it('should support abs', () => {
    const out = render(
      'You are {@math key=age method="abs"}',
      { age: -20 },
    );
    expect(out).toEqual('You are 20');
  });

  it('should not support unknown match', () => {
    const out = render(
      'You are {@math key=age method="brr" operand=5}done',
      { age: -20 },
    );
    expect(out).toEqual('You are done');
  });

  it('should process an HTML template', () => {
    const out = render(
      '<div>\n    <p>Hello, {name}</p>\n</div>',
      { name: 'Jim' },
    );
    expect(out).toEqual('<div><p>Hello, Jim</p></div>');
  });

  it('should support inline definitions', () => {
    const out = render(
      '{<name}xxx{/name}Hello, {+name}Generic{/name}!',
      {},
    );
    expect(out).toEqual('Hello, xxx!');
  });

  it('should support references', () => {
    const out = render(
      '{+name}All{/name}: {<name}xxx{/name}Hello, {+name}Generic{/name}!{<name}zzz{/name}',
      {},
    );
    expect(out).toEqual('zzz: Hello, zzz!');
  });

  it('should support default reference', () => {
    const out = render(
      'Hello, {+greeting}{name}{/greeting}!',
      { name: 'World' },
    );
    expect(out).toEqual('Hello, World!');
  });

  it('should support reference without default', () => {
    const out = render(
      'Hello, {+greeting/}!',
      {},
    );
    expect(out).toEqual('Hello, !');
  });

  it('should support reference and definition', () => {
    const out = render(
      'Hello, {+greeting /}{<greeting}Bob{/greeting}!',
      {},
    );
    expect(out).toEqual('Hello, Bob!');
  });

  it('should support references in a section', () => {
    const out = render(
      'Hello, {+name}None{/name}{#seasons}{<name boo=name}{boo} {/name}{/seasons}',
      { seasons: [{ name: 'Summer' }, { name: 'Winter' }] },
    );
    expect(out).toEqual('Hello, Summer Winter ');
  });

  describe('Contextual data access', () => {
    const data = {
      first: 'William',
      last: 'Smith',
      info: {
        age: 37,
        gender: 'M',
        first: 'Bill',
        children: ['Duane', 'Jon', 'Elvis'],
      },
    };

    it('should test falseness (?)', () => {
      const out = render(
        '{#values}{?.}1{:else}0{/.}{/values}',
        { values: ['', false, null, undefined, []] },
      );
      expect(out).toEqual('00000');
    });

    it('should test truthiness (?)', () => {
      const out = render(
        '{#values}{?.}1{:else}0{/.}{/values}',
        { values: [0, '0', 'null', 'undefined', 'false', {}, { a: 1 }, true] },
      );
      expect(out).toEqual('11111111');
    });

    it('should test falseness (#)', () => {
      const out = render(
        '{#values}{#.}1{:else}0{/.}{/values}',
        { values: ['', false, null, undefined, []] },
      );
      expect(out).toEqual('00000');
    });

    it('should test truthiness (#)', () => {
      const out = render(
        '{#values}{#.}1{:else}0{/.}{/values}',
        { values: [0, '0', 'null', 'undefined', 'false', {}, { a: 1 }, true] },
      );
      expect(out).toEqual('11111111');
    });

    it('should return empty string for undefined', () => {
      const templ = 'Hello{?names}{names[3]}{/names}!';
      const out = render(templ, { names: ['Kitty', 'Joe'] });
      expect(out).toBe('Hello!');
    });


    it('should access data in the current context', () => {
      const templ = 'Hello, {first} {last}!';
      const out = render(templ, data);
      expect(out).toBe('Hello, William Smith!');
    });

    it('should access data in a nested context', () => {
      const templ = '{#info}Hello, {first} {last}{/info}!';
      const out = render(templ, data);
      expect(out).toBe('Hello, Bill Smith!');
    });

    it('should access a nested object', () => {
      const templ = 'Hello, {first} {last}, age {info.age}!';
      const out = render(templ, data);
      expect(out).toBe('Hello, William Smith, age 37!');
    });

    it('should access a nested array', () => {
      const templ = 'Hello, {first} {last}, with first child {info.children[0]}!';
      const out = render(templ, data);
      expect(out).toBe('Hello, William Smith, with first child Duane!');
    });

    it('should access a nested array as section', () => {
      const templ =
        'Hello, {first} {last}, with second child {#info.children[1]}{.}{/info.children[1]}!';
      const out = render(templ, data);
      expect(out).toBe('Hello, William Smith, with second child Jon!');
    });

    it('complex reference', () => {
      const templ = 'Hello, {a.b[0][1].c}!';
      const out = render(templ, {
        a: { b: [[{ c: 'Tree' }, { c: 'Smith' }], { c: 'Jones' }, { c: 'Brown' }] },
      });
      expect(out).toBe('Hello, Smith!');
    });
  });

  describe('More fun with paths', () => {
    const data = {
      name: 'root',
      anotherName: 'root2',
      A: {
        name: 'Albert',
        B: {
          name: 'Bob',
        },
      },
    };

    it('section reference', () => {
      const templ = '{#A}{name}{/A}';
      const out = render(templ, data);
      expect(out).toBe('Albert');
    });

    it('section parent reference', () => {
      const templ = '{#A}{anotherName}{/A}';
      const out = render(templ, data);
      expect(out).toBe('root2');
    });

    it('dot notation', () => {
      const templ = '{A.B.name}';
      const out = render(templ, data);
      expect(out).toBe('Bob');
    });

    it('nested section reference', () => {
      const templ = '{#A.B}{name}{/A.B}';
      const out = render(templ, data);
      expect(out).toBe('Bob');
    });

    it('parent reference', () => {
      const templ = '{#A.B}{A.name}{/A.B}';
      const out = render(templ, data);
      expect(out).toBe('Albert');
    });

    it('parent reference using section', () => {
      const templ = '{#A.B}{#A}{name}{/A}{/A.B}';
      const out = render(templ, data);
      expect(out).toBe('Albert');
    });

    it('current context only', () => {
      const templ = '{#A.B}Hello {.name}!{/A.B}';
      const out = render(templ, data);
      expect(out).toBe('Hello Bob!');
    });

    it('not in current context', () => {
      const templ = '{#A.B}Hello {.anotherName}!{/A.B}';
      const out = render(templ, data);
      expect(out).toBe('Hello !');
    });

    it('look higher in context', () => {
      const templ = '{#A.B}Hello {anotherName}!{/A.B}';
      const out = render(templ, data);
      expect(out).toBe('Hello root2!');
    });

    it('should process embedded quotes', () => {
      expect(render(
        '{@eq key="s\'iz\'e" value="s\'iz\'e"}Embedded{:else}No{/eq}',
        {},
      )).toEqual('Embedded');
    });

    it('should process nested arrays', () => {
      expect(render(
        'Number {a[b[2]]}',
        { a: [9, 4], b: [3, 6, 1] },
      )).toEqual('Number 4');
    });

    it('should accept single quotes strings', () => {
      expect(render(
        "{@eq key=word value='hello'}Hello{:else}Bye{/eq}",
        { word: 'hello' },
      )).toEqual('Hello');
    });
  });

  describe('Error conditions', () => {
    it('should throw on plugin error', () => {
      registerPlugin('@bad', () => {
        throw new Error('Bad Helper');
      });
      expect(() =>
        render(
          'This should {@bad} throw an error.',
          {},
        )).toThrow();
    });

    it('should throw on mismatched quote', () => {
      expect(() => {
        render('{"Hello}', {});
      }).toThrow();
    });

    it('should throw on mismatched tag', () => {
      expect(() => {
        render(
          '{?pet}Enjoy the pet{/oops}',
          { pet: true },
        );
      }).toThrow('Mismatched tag: {/pet}');
    });

    it('should throw on mismatch brackets', () => {
      expect(() => {
        render(
          'Number {a[b[2]}',
          { a: [9, 4], b: [3, 6, 1] },
        );
      }).toThrow('Mismatched brackets');
    });

    it('should select any', () => {
      expect(render(
        'You are {@select key=size}{@any}Very {/any}{@eq value="s"}Small{/eq}{@eq value="l"}Large{/eq}{@none}non-descript{/none}{/select}!',
        { size: 'l' },
      )).toEqual('You are Very Large!');
    });

    it('should select none', () => {
      expect(render(
        'You are {@select key=size}{@any}Very {/any}{@eq value="s"}Small{/eq}{@eq value="l"}Large{/eq}{@none}non-descript{/none}{/select}!',
        { size: 'm' },
      )).toEqual('You are non-descript!');
    });
  });
});
